package taskapp.demo.mapper;

import org.springframework.stereotype.Component;
import taskapp.demo.dto.TaskDto;
import taskapp.demo.entity.Task;
import taskapp.demo.repository.TaskRepository;

import java.util.Optional;

@Component
public class TaskMapper {

    private final TaskRepository taskRepository;

    public TaskMapper(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }


    public Task toEntity(TaskDto taskDto) {

        Task task = new Task();
        task.setDocDate(taskDto.getDocDate());
        task.setName(taskDto.getName());
        Optional<Task> byId = taskRepository.findById(taskDto.getParentDto().getParentDto().getId());
        byId.ifPresent(task::setParentId);
        return task;

    }

    public TaskDto toDto(Task task) {

        TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setDocDate(task.getDocDate());
        taskDto.setName(task.getName());
        taskDto.setParentDto(task.getParentId());
        return taskDto;
    }
}
